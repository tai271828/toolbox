#!/bin/bash
commits="f8408d2b79b834f79b6c578817e84f74a85d2190 bedd04e4aa1434d2f0f038e15bb6c48ac36876e1 34932a6033be3c0088935c334e4dc5ad43dcb0cc"

for commit in ${commits}
do
    echo ${commit}
    get_git_tag_by_hash 'v[[:digit:]]*' ${commit} 'v[[:digit:]]*'
    echo
done
