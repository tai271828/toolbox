#!/usr/bin/env bash
PKG="plainbox-provider-checkbox"

sudo add-apt-repository ppa:checkbox-dev/ppa -y
sudo apt update
# may upgrade your target package so you can not test your target version
#sudo apt install -y ${PKG} checkbox-ng canonical-certification-server

sudo checkbox-cli run com.canonical.certification::device com.canonical.certification::virtualization/kvm_check_vm
apt-cache show plainbox-provider-checkbox=$( dpkg -l ${PKG} | grep ii | awk '{print $3}' )
lsb_release -a
uname -a
dpkg -l plainbox-provider-checkbox
dpkg -l qemu-efi-aarch64
