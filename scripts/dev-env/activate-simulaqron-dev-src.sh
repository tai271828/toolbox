# source me
#
# edit me
pushd ${HOME} > /dev/null

PROJECT_WORKING_ROOT="${HOME}/work-my-projects/simulaqron-1st-look/dev-src/"

source ./activate-dev-env.sh

export PYTHONPATH=${SIMULAQRON_PROJECT_ROOT}/SimulaQron:$PYTHONPATH
alias simulaqron=${SIMULAQRON_PROJECT_ROOT}/SimulaQron/simulaqron/SimulaQron.py

popd > /dev/null
