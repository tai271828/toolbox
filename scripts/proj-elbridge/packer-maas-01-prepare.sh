# build maas image for BF2 with packer-maas approach
# see https://discourse.maas.io/t/maas-and-dpus/6390

work_dir_name="packer-maas"
work_dir=${HOME}/${work_dir_name}/ubuntu

sudo apt install qemu-utils qemu-system ovmf cloud-image-utils -y

# installation instruction of latest packer from https://developer.hashicorp.com/packer/downloads?product_intent=packer
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install packer

pushd $HOME
git clone https://github.com/canonical/${work_dir_name}.git
popd

pushd ${work_dir}
sudo packer init .
sudo packer build -var customize_script=setup-bluefield.sh -var ubuntu_series=jammy -var architecture=arm64 -only 'cloudimg.*' .
popd
