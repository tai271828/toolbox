#!/usr/bin/python

import pdb

input_filename = 'fresh_sub_ch.txt'
input_filenema_init = input_filename + '.init'
output_filename = input_filename + '.out'

def write_list_as_str(output_file, list_sentence):
    for x in list_sentence:
        output_file.write(x)
    output_file.write("\n")

tmp_filename = output_filename
output_filename = input_filenema_init
with open(input_filename) as input_file, open(output_filename, 'w') as output_file :
    data = input_file.readlines()

    for line in data:
        if ((line[:2] == '01') or (line[:2] == '02')):
            output_file.write(line)
        elif (line[0] == "\n" and len(line) == 1):
            pass
        else:
            output_file.write(line)
output_filename = tmp_filename

with open(input_filename) as input_file, open(output_filename, 'w') as output_file :
    data = input_file.readlines()

    prev_onetime_sub = []
    post_onetime_sub = []
    following_sub = []
    for line in data:
        if ((line[:2] == '01') or (line[:2] == '02')):
            print 'breakpoint: 2 line 2'
            print prev_onetime_sub
            print post_onetime_sub
            print following_sub
            #pdb.set_trace()
            post_onetime_sub = prev_onetime_sub + following_sub
            prev_onetime_sub = [line.rstrip()]
            write_list_as_str(output_file, post_onetime_sub)
            post_onetime_sub = []
            following_sub = []
        elif (line[0] == "\n" and len(line) == 1):
            pass
        else:
            print 'breakpoint: lastelse'
            print prev_onetime_sub
            print post_onetime_sub
            print following_sub
            #pdb.set_trace()
            following_sub.append(' ' + line.rstrip())
    post_onetime_sub = prev_onetime_sub + following_sub
    write_list_as_str(output_file, post_onetime_sub)
