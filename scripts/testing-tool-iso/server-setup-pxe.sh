pxe_ip=10.229.58.39
pxe_url="http://${pxe_ip}/"
image_name_arm64=jammy-live-server-arm64.iso
image_url_arm64=http://cdimage.ubuntu.com/ubuntu-server/daily-live/20220421/${image_name_arm64}
image_name_amd64=jammy-live-server-amd64.iso
image_url_amd64=http://cdimage.ubuntu.com/ubuntu-server/daily-live/20220421/${image_name_amd64}

pushd ${HOME}/ubuntu-autoinstall-pxe-server
echo "Make sure we are using the latest code base..."
#git submodule update --recursive --remote
git pull --recurse-submodules
popd

pushd ${HOME}
wget ${image_url_arm64}
popd
pushd ${HOME}/ubuntu-autoinstall-pxe-server
echo "setting up pxe server..."
sudo ./setup-pxe-server.sh --url ${pxe_url}/${image_name_arm64} --iso ${HOME}/${image_name_arm64} --autoinstall-url  ${pxe_url}
popd

#pushd ${HOME}
#wget ${image_url_amd64}
#popd
#pushd ${HOME}/ubuntu-autoinstall-pxe-server
#echo "setting up pxe server..."
#sudo ./setup-pxe-server.sh --url ${pxe_url}/${image_name_amd64} --iso ${HOME}/${image_name_amd64} --autoinstall-url  ${pxe_url}
#popd

#echo
#echo "dumping code base info including the submodules"
#pushd ${HOME}/ubuntu-autoinstall-pxe-server
#git show
#popd
#pushd ${HOME}/ubuntu-autoinstall-pxe-server/ubuntu-server-netboot
#git show
#popd
#echo

echo "============================================="
echo "debugging tips:"
echo "1. test the core tool: ./ubuntu-server-netboot --url http://cdimage.ubuntu.com/ubuntu-server/daily-live/20220418.2/jammy-live-server-arm64.iso"
echo "2. test the wrapper:   sudo ./setup-pxe-server.sh --url http://cdimage.ubuntu.com/ubuntu-server/daily-live/20220418.2/jammy-live-server-arm64.iso"
echo "2-1. test the wrapper: sudo ./setup-pxe-server.sh --url http://cdimage.ubuntu.com/ubuntu-server/daily-live/20220418.2/jammy-live-server-arm64.iso --iso /home/ubuntu/jammy-live-server-arm64.iso"
echo "2-2. test the wrapper: sudo ./setup-pxe-server.sh --url http://cdimage.ubuntu.com/ubuntu-server/daily-live/20220418.2/jammy-live-server-arm64.iso --iso /home/ubuntu/jammy-live-server-arm64.iso --autoinstall-url http://10.228.68.115/"
echo "3. test this wrapper"
echo

echo "============================================="
echo Several check points:
echo 1. using bionic?
echo 2. the file contents of tftp and apache servers
ls /var/lib/tftpboot/
echo
ls /var/www/html
echo
echo 3. code base info and versioin including the submodules
