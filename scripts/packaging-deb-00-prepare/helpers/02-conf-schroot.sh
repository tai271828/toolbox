#!/usr/bin/env bash
SCHROOT_CONF_TRANSITIONAL="/tmp/schroot-build.conf"
WORKING_DIR="${HOME}/my-local-installation"
CHROOT_DIR="${WORKING_DIR}/debootstrap-chroot-${DISTRIBUTION}-${ARCH}-${RELEASE}"

mkdir -p "${WORKING_DIR}"

REPOSITORY_URL=""
if [ "${DISTRIBUTION}" == "debian" ]
then
  sudo debootstrap ${RELEASE} "${CHROOT_DIR}" http://deb.debian.org/debian/
elif [ "${DISTRIBUTION}" == "ubuntu" ] && [ "${ARCH}" == "amd64" ]
then
  sudo debootstrap ${RELEASE} "${CHROOT_DIR}" http://us.archive.ubuntu.com/ubuntu/
elif [ "${DISTRIBUTION}" == "ubuntu" ] && [ "${ARCH}" == "arm64" ]
then
  sudo debootstrap ${RELEASE} "${CHROOT_DIR}" http://ports.ubuntu.com/ubuntu-port
else
  exit 1
fi

rm -f ${SCHROOT_CONF_TRANSITIONAL}

cat << EOT >> ${SCHROOT_CONF_TRANSITIONAL}
[${RELEASE}]
description=${DISTRIBUTION} ${RELEASE} (deboostrap by tai)
directory=${CHROOT_DIR}
root-users=${USER}
users=${USER}
type=directory

EOT

sudo bash -c "cat ${SCHROOT_CONF_TRANSITIONAL} >> /etc/schroot/chroot.d/deb-build.conf"

echo
echo "toolbox: you may need to edit /etc/schroot/chroot.d/deb-build.conf like this (so we appended it for you):"
cat ${SCHROOT_CONF_TRANSITIONAL}

echo
echo "toolbox: you need to logout to make schroot effective"
echo

