#!/bin/bash

set -x


modinfo nvidia

echo "Show nv ko..."
lsmod | grep -e mlx -e nv -e nvidia
dkms status

echo "Show installed packages..."
dpkg -l nvidia-fabricmanager* mlnx-ofed-kernel-only nvidia-utils-* nvidia-kernel-source-*
dpkg -l | grep nvidia
sudo apt list "*nvidia*" --installed

cat /proc/driver/nvidia/version

nvidia-smi -L
nvidia-smi -h | grep 'NVIDIA System Management Interface'

echo "Show kernel and distro info..."
uname -a
lsb_release -a
