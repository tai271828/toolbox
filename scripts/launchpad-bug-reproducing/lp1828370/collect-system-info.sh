interface="enp133s0f0"

uname -a
lsb_release -a
ethtool -i ${interface}
ip -d addr show dev ${interface}

echo "/sys/class/net/${interface}/device/sriov_numvfs" value is:
cat /sys/class/net/${interface}/device/sriov_numvfs

sudo lspci -vvv

dpkg -l
