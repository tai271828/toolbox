#!/bin/bash
#
# Using ImageMagick, a software suite, to batch resize images.
#
# Use
# $ sudo apt-get install imagemagick
# to get the ImageMagick suite
#

workdir=/home/thho/Photos/2010/03/27/

echo Are you sure you want to resize images under the directory including sub-directories,
echo
echo $workdir
echo

read -p "Please backup before resizing and confirm the working directory again [PATH]:" workdir

echo Now the working directory is
echo 
echo $workdir
echo

#find $workdir -name "*.jpg"
find $workdir -name "*.jpg" -size +1M -exec mogrify -resize 60%x60% {} \;
find $workdir -name "*.JPG" -size +1M -exec mogrify -resize 60%x60% {} \;
