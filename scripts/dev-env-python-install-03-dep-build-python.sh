#!/usr/bin/env bash
#
# Get ready to build python with Ubuntu 20.04 (Focal)
#
pkgs_must="build-essential libzip-dev libbz2-dev libssl-dev"
pkgs_optional="libreadline-dev libsqlite3-dev"
pkgs="${pkgs_must} ${pkgs_optional}"

sudo apt-get install -y ${pkgs}
