#!/bin/bash
#
# ubuntu@saenger:~$ for idx in {1..30}; do echo $idx run; sudo ./toolbox/scripts/testing-tool-kernel-and-lp/kas-hpre.sh; done
#

PCI_DEVICE_1=/sys/devices/pci0000\:74/0000:74:01.0/0000:75:00.0/reset
PCI_DEVICE_2=/sys/devices/pci0000\:b4/0000:b4:01.0/0000:b5:00.0/reset


echo 1 > ${PCI_DEVICE_1}
sleep 1
echo 1 > ${PCI_DEVICE_2}

# benchmark tool
# https://github.com/inikep/lzbench
/home/ubuntu/lzbench/lzbench -t16,16 -ezlib ./lzbench.tar

