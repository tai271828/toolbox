#!/usr/bin/env python3
from socket import socket, AF_PACKET, SOCK_RAW
s = socket(AF_PACKET, SOCK_RAW)

# We're putting together an ethernet frame here, 
# but you could have anything you want instead
# Have a look at the 'struct' module for more 
# flexible packing/unpacking of binary data
# and 'binascii' for 32 bit CRC
s.bind(("eno3", 0))
#src_addr = "\x00\x18\x82\x2f\x02\x01"
#dst_addr = "\x00\x18\x82\x2f\x02\x01"

#s.bind(("eno3", 0))
#src_addr = "\x00\x18\x82\x2f\x02\x02"
#dst_addr = "\x00\x18\x82\x2f\x02\x02"

#s.bind(("eno3", 0))
src_addr = "\x00\x18\x82\x2f\x02\x07"
dst_addr = "\x00\x18\x82\x2f\x02\x07"

#payload = ("["*30)+"PAYLOAD"+("]"*30)
#payload = ("s")
payload = ("ssssddddddddddff")
#checksum = "\x1a\x2b\x3c\x4d"
ethertype = "\x08\x01"

#send_data = bytes(dst_addr+src_addr+ethertype+payload+checksum, "utf8")
send_data = bytes(dst_addr + src_addr + ethertype + payload, "utf8")
#send_data = bytes("s", "utf8")
print(f"The length of the data in bytes: {len(send_data)}")
s.send(send_data)
