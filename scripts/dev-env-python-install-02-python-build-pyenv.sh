#!/usr/bin/env bash
#
# Get ready to use python-build pluging of pyenv
#

# Install pyenv
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash

echo
echo "By default, pyenv source will be put in ${HOME}/.pyenv ."
echo "pipenv will detect if pyenv is available and use its python-build plugin to build Python."
echo
echo "You will need to update pyenv source (by git pull) when new Python is released and you want to build it."

