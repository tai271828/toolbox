#!/usr/bin/env bash

git clone https://git.launchpad.net/~taihsiangho/+git/toolbox -b dev --depth 1 --recurse-submodules -j4

echo
echo "Performing the initial setup:"
echo

echo 'source ${HOME}/toolbox/config/bash/hook-all.sh' >> ${HOME}/.bash_aliases

mkdir -p ${HOME}/.local/bin
pushd ${HOME}/.local/bin
ln -vnfs /home/tai271828/toolbox/scripts/labkey ./

ln -vnfs /home/tai271828/toolbox/scripts/launcher/zim ./
ln -vnfs /home/tai271828/toolbox/scripts/pyinvoicer ./
ln -vnfs /home/tai271828/toolbox/scripts/pyinvoicer ./invoicer
ln -vnfs /home/tai271828/toolbox/scripts/ctimer ./
popd
