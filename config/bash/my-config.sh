#!/bin/bash
# hg version control setting
if [ ! -f $HOME/.hgrc ]; then
    cp $MYTOOLBOX/config/hg/hgrc $HOME/.hgrc
fi

# for touchpads of laptops
# use three-finger-touch as gnome paste
if [ `hostname` == 'nanhu' ] || [ `hostname` == 'hamster' ] || [ `hostname` == 'mtlb' ] || [ `hostname` == 'ibaho' ]; then
    synclient TapButton3=2;
fi
