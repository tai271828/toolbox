#!/bin/bash


# BIOS:
# looking for something like
#
#   Power, Hardware Power Management
#   S4/S5 Wake on LAN
#
# is on


# make sure the kernel support the RCT now
echo "Kernel configuration about RCT:"
echo "Command: grep -i rtc /boot/config-$(uname -r)"
grep -i rtc /boot/config-$(uname -r)

echo
echo 
echo "Command: grep -i rtc /var/log/dmesg"
#grep -i rtc /var/log/kern.log
#grep -i rtc /var/log/messages
grep -i rtc /var/log/dmesg


# make sure the driver module was loaded 
echo
echo
echo "Command: ls -alh /dev/rtc"
ls -alh /dev/rtc
echo "Command: ls /proc/driver/rtc"
ls /proc/driver/rtc
echo "Command: ls /sys/class/rtc/rtc0/wakealarm"
ls /sys/class/rtc/rtc0/wakealarm


# record the information
echo
echo
echo "Command: cat /proc/driver/rtc"
cat /proc/driver/rtc
echo "Command: cat /sys/class/rtc/rtc0/wakealarm"
cat /sys/class/rtc/rtc0/wakealarm

# grep the information for diagnosis
source rtc-wakealrm-diagnosis.sh
