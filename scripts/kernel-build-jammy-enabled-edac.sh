#!/usr/bin/bash
pushd ${HOME}
git clone https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/jammy jammy.orig
mkdir kernel-build-jammy
cp -r ./jammy.orig ./kernel-build-jammy

fakeroot debian/rules clean
fakeroot debian/rules binary-generic skipabi=true skipmodule=true
fakeroot debian/rules binary-indep
fakeroot debian/rules clean
