#!/usr/bin/env bash

#https://askubuntu.com/a/769429/305641
#sudo sed -i '/^#\sdeb-src /s/^#//' "/etc/apt/sources.list"
sudo sed -i '/^#\sdeb-src /s/^# *//' "/etc/apt/sources.list"
sudo apt update
sudo apt install -y devscripts debhelper equivs
sudo apt-get build-dep -y linux
# jammy
#sudo apt install -y flex bison

# ~/focal$ debian/rules clean
# sudo mk-build-deps -i
