#!/bin/sh

set -e -x

ssh lakitu lxc delete --force dannf-scalebot || :
ssh -t lakitu lxc launch ubuntu:focal --vm dannf-scalebot -t c8-m16
# Wait for VM to become ready
for _ in $(seq 300); do
    if ssh lakitu lxc exec dannf-scalebot -- /bin/true; then
	break
    fi
    sleep 1
done

# Grow the disk and reboot for it to take effect
ssh lakitu lxc config device override dannf-scalebot root size=36GB
ssh lakitu lxc exec dannf-scalebot -- reboot

# Wait for it to actually shutdown
sleep 5

# Wait for VM to become ready and ubuntu user to exist
for _ in $(seq 300); do
    if ssh lakitu lxc exec dannf-scalebot -- id ubuntu; then
	break
    fi
    sleep 1
done
ssh lakitu lxc exec dannf-scalebot -- sudo -u ubuntu ssh-import-id-lp dannf
ip="$(ssh lakitu lxc exec dannf-scalebot -- ip route | grep '^default ' | cut -d' ' -f9)"

tmpgit="$(mktemp -d)"
xstartup_tmp="$(mktemp)"

cleanup() {
    rm -rf "${tmpgit}"
    rm -f "$xstartup_tmp"
}
trap cleanup EXIT

cat > ~/.ssh/config-scalebot <<EOF
HostName $ip
User ubuntu
ProxyCommand ssh lakitu nc -q0 %h %p
StrictHostKeyChecking no
UserKnownHostsFile=/dev/null
EOF

ssh scalebot sudo snap install lxd --channel=stable
ssh scalebot sudo lxd init --auto
# Juju 2.9.26 says:
# ERROR profile "default": juju does not support IPv6. Disable IPv6 in LXD via:
#	lxc network set lxdbr0 ipv6.address none
ssh scalebot lxc network set lxdbr0 ipv6.address none
ssh scalebot lxc config set core.trust_password insecure
ssh scalebot sudo snap install juju --classic
ssh scalebot sudo snap install charmcraft --classic
ssh scalebot juju bootstrap localhost


lxd_ip="$(ssh scalebot ip route | grep ' dev lxdbr0 ' | cut -d' ' -f9)"
git clone git+ssh://dannf@git.launchpad.net/scalebot "$tmpgit/scalebot"
sed -i "s,endpoint: .*,endpoint: https://${lxd_ip}:8443," \
    "$tmpgit/scalebot/labs/remote-lxd/clouds.yaml"
git clone git+ssh://git.launchpad.net/~ce-hyperscale/charms/+source/scalebot-jenkins "$tmpgit/scalebot-jenkins"
rsync -avPz "$tmpgit"/* scalebot:~


#
# Start a desktop environment over vnc so we can connect to jenkins
#
ssh scalebot sudo apt update
ssh scalebot sudo apt install -y \
    tigervnc-standalone-server icewm xterm firefox emacs
ssh scalebot mkdir .vnc
ssh scalebot touch .vnc/passwd
ssh scalebot chmod 600 .vnc/passwd
echo iforgot | ssh scalebot vncpasswd -f | ssh scalebot tee .vnc/passwd > /dev/null

cat > "$xstartup_tmp" <<EOF
#!/bin/sh

xrdb /home/dannf/.Xresources
xsetroot -solid grey
xterm &
icewm &
EOF

chmod 755 "$xstartup_tmp"

scp "$xstartup_tmp" scalebot:~/.vnc/xstartup

ssh scalebot vncserver -localhost no

echo "To setup a tunnel to the vncserver:"
echo "  ssh -L 20800:localhost:5901 scalebot"
