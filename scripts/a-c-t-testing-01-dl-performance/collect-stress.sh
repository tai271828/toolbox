#!/bin/bash
data_dir=$1

grep tensor_flow_cnn_resnet_config1_average *-stdout.log | awk {'print $5'}
grep tensor_flow_cnn_resnet_config1_maximum_error *-stdout.log | awk {'print $5'}

echo "Expected PASS: 100"
echo "Actual PASS  :" $( grep "PASS: test passes specified performance thresholds" ${data_dir}/*.log | wc -l )
