#!/bin/bash
macaddr=$(echo $HOSTNAME | md5sum | sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')
memory=8192
interface_on_host=tap3

#mapfile -t part < <(sudo kpartx -asv ./nic-ubuntu-jammy-bf.img | cut -d' ' -f3)
#echo ${part[2]}
#sudo mount /dev/mapper/loop6p2 ./p2
#
#dd if=/dev/zero of=kernel-hd bs=1M count=4096
#mkfs.ext4 kernel-hd
#sudo mount kernel-hd ./mnt/
#sudo cp -a ./p2/* ./mnt/
#sudo umount ./mnt

# rw for root=/dev/vda matters since cloud-init would try to write /var/lib/cloud/data

# https://gist.github.com/pojntfx/25c611c64d89ef9bc5c76074a460bf44
#sudo su
#ip tuntap add tap0 mode tap user root
#ip addr add dev tap0 10.0.0/24
#ip link set tap0 up
#
#cat > /etc/dhcp/dhcpd.conf <<EOF
#subnet 10.0.0.0 netmask 255.255.255.0 {
#        authoritative;
#        range 10.0.0.1 10.0.0.254;
#        default-lease-time 3600;
#        max-lease-time 3600;
#        option subnet-mask 255.255.255.0;
#        option broadcast-address 10.0.0.255;
#        option routers 10.0.0.0;
#        option domain-name-servers 8.8.8.8;
#        option domain-name "example.com";
#}
#EOF
#touch /var/lib/dhcp/dhcpd.leases
#chown $(whoami) /etc/dhcp
#chown $(whoami) /var/lib/dhcp/
#dhcpd -f tap0

sudo qemu-system-aarch64 \
    -enable-kvm -M virt \
    -m $memory -cpu host \
    -nographic \
    -kernel vmlinuz \
    -initrd initrd.img \
    -drive file=kernel-hd,format=raw,index=0,media=disk \
    -append "root=/dev/vda rw" \
    -netdev type=tap,id=net0,ifname=$interface_on_host,script=no,downscript=no \
    -device virtio-net-device,netdev=net0,mac=$macaddr
