#!/bin/bash
#
# mk-disk-img.sh
#
# usage:
#   sudo bash mk-disk-img.sh 2>&1 mk-disk-img.sh.log
#

date_label=`date +%F`
backup_dest_dir="/media/tai271828/2tb/${date_label}"

mkdir -p ${backup_dest_dir}

for target in sda sdb
do
    echo "dumping disk information..."
    for parted_script in 'compact' 's' 'chs'
    do
        parted -s /dev/${target} unit ${parted_script} print > ${backup_dest_dir}/${target}.parted.unit.${parted_script}.log
    done
    echo "cloning disk image: /dev/${target}"
    dd if=/dev/${target} conv=sync,noerror bs=64K | gzip -c > ${backup_dest_dir}/${target}.img.gz
done
md5sum ${backup_dest_dir}/*.img.gz > ${backup_dest_dir}/img.gz.md5

echo 'done!'

