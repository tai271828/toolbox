#!/bin/bash
target_root=$HOME/Dropbox/bacterium-symlink/irc-logs
target_name="`date +%F`-`hostname`"
target="$target_root/$target_name"
src="$HOME/.xchat2"

echo "copying IRC - xchat2..."
if [ -d $target ]; then
    rm -rf ${target}.bak
    mkdir -p ${target}.bak
    mv $target ${target}.bak/
fi
mkdir -p $target
cp -a $src ${target}/
echo "copying IRC - xchat2...done!"

