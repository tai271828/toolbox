#!/usr/bin/env bash
#
# tested distribution: ubuntu focal (20.04)
#
# reference of build recipe
# https://code.launchpad.net/~checkbox-dev/+recipe/plainbox-provider-sru-daily
#

set -e

PKG_NAME=plainbox-provider-checkbox
PKG_VERSION="0.63.0"
WORKING_ROOT=${HOME}/build-plainbox-provider-checkbox-deb
SRC_REMOTE_UPSTREAM=https://git.launchpad.net/${PKG_NAME}
SRC_REMOTE_DEB=https://git.launchpad.net/~checkbox-dev/${PKG_NAME}/+git/packaging
SRC_UPSTREAM=${WORKING_ROOT}/${PKG_NAME}
SRC_DEB=${WORKING_ROOT}/packaging
SRC_TARBALL=${PKG_NAME}_${PKG_VERSION}.orig.tar.gz
SRC_PATCH_ROOT=${WORKING_ROOT}/patch-factory
DEP_PKG="devscripts dh-make dh-exec quilt git-buildpackage"

sudo add-apt-repository ppa:checkbox-dev/ppa -y
sudo apt update
sudo apt install -y checkbox-ng plainbox-provider-checkbox canonical-certification-server
sudo apt install -y ${DEP_PKG}

mkdir -p "${WORKING_ROOT}"
cd "${WORKING_ROOT}"

git clone ${SRC_REMOTE_UPSTREAM}
git clone ${SRC_REMOTE_DEB}

echo #############################################################################################
echo # Create source tarball
echo #############################################################################################
pushd "${SRC_UPSTREAM}"
#git checkout v"${PKG_VERSION}"
popd

"${SRC_UPSTREAM}"/manage.py sdist
cp "${SRC_UPSTREAM}"/dist/${PKG_NAME}-*.tar.gz "${SRC_DEB}"/../"${SRC_TARBALL}"

echo #############################################################################################
echo # Prepare git tree for packaging
echo #############################################################################################
cd "${SRC_DEB}"

git branch --all

# use the git-dpm workflow
#git-dpm import-new-upstream ../"${SRC_TARBALL}"
#git branch --all
#
#pristine-tar commit ../"${SRC_TARBALL}"
#git branch --all

# use the gbp workflow. it does something similar to git-dpm/pristine-tar flow but apply the default tag of gbp style
# Ref: Debian Git package of Python package debian wikipage https://wiki.debian.org/Python/GitPackaging
git branch upstream
gbp import-orig --pristine-tar ../"${SRC_TARBALL}" --no-interactive
git --no-pager log --graph -3
git branch --all

echo #############################################################################################
echo # Create the target patch
echo #############################################################################################
git clone -b master https://git.launchpad.net/~dannf/plainbox-provider-checkbox/+git/lp1940112 "${SRC_PATCH_ROOT}"
pushd "${SRC_PATCH_ROOT}"
git format-patch -1
popd

# Ref: https://www.debian.org/doc/manuals/debmake-doc/ch05.en.html#patches
#  The diff command
#
#  See Section 4.9.1, “Patch by diff -u”
#  Primitive but versatile method
#
#  Patches may come from other distros, mailing list postings, or cherry-picked patches from the upstream git repository with the “git format-patches” command
#  Missing the .pc/ directory
#  Unmodified upstream source tree
#  Manually update the debian/patches/series file
echo "Preparing the patch set based on gbp workflow"
mkdir "${SRC_DEB}"/debian/patches/
cp "${SRC_PATCH_ROOT}"/*.patch "${SRC_DEB}"/debian/patches
( cd "${SRC_DEB}"/debian/patches/; ls *.patch > "${SRC_DEB}"/debian/patches/series )

echo #############################################################################################
echo # Build deb
echo #############################################################################################
# Choosing one of the following methods is sufficient
#
# build with the classic tool and plain environment
#build_cmd="dpkg-buildpackage -us -uc"

# build with the classic tool and fakeroot environment (assuming your chroot env is setup)
#build_cmd="dpkg-buildpackage -us -uc -rfakeroot"

# build with sbuild (assuming your chroot env is setup)
#build_cmd="sbuild"

# build with gbp (assuming your tree is setup) and sbuild (assuming your chroot env is setup)
build_cmd="gbp buildpackage --git-ignore-new --git-builder=sbuild"

# invoke building according to your choice
echo "${build_cmd}"
${build_cmd}

echo #############################################################################################
echo # Post-work house-cleaning after building deb
echo #############################################################################################
# cleaning
# git restore ./; git clean -df
# rm -f "${SRC_DEB}"/../*.debian.tax.xz "${SRC_DEB}"/../*.dsc
