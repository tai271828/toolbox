#!/bin/bash


echo
echo
echo "use rtcwake to test..."
# please note the option -l to use local time or not
# check you are using UCT or local time (8hr difference in Taiwan)
rtcwake -m no -t $(date +%s --date 'now + 1 minute')
# or use
#rtcwake -u -s -m no
shutdown -h now
