#!/usr/bin/env python3
import os
import time
from multiprocessing import Process
from subprocess import call as system_call

hosts = {'host1': '8.8.8.8', 'host2': '8.8.4.4'}

DEVNULL = open(os.devnull, 'w')

# Define a function for the thread
def ping_host(host, hostname, count=10, delay=1):
    """
    Print response message if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP)
    request even if the host name is valid.

    If the count number is 0, it means endless ping.
    """
    if count == 0:
        command = ['ping', host]
    else:
        param = '-c %s' % count
        command = ['ping', param, host]

    while True:
        result = system_call(command, stdout=DEVNULL)
        if result == 0:
            print("{} {}: Responded".format(hostname, host))
        else:
            print("{} {}: Responded None".format(hostname, host))

        time.sleep(5)


processes = []
for hostname in hosts:
    try:
        p = Process(target=ping_host, args=(hosts.get(hostname), hostname, 5, ))
        processes.append(p)
    except:
        print("Error: unable to create a process")

for process in processes:
    process.start()

for process in processes:
    process.join()


# Never stop this program
while True:
    time.sleep(30)
