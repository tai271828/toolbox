#!/bin/bash
set -x

LXD_OS_VER=20.04

find_latest_cuda_container_tag_by_branch() {
    local branch="$1" # e.g. 11.4
    local v2_api='https://registry.hub.docker.com/v2/repositories/nvidia/cuda/tags'
    local v2_api_page_size=100
    local total_tag_number=$( curl -L -s ${v2_api} | jq '."count"' )
    local last_page_number=$((${total_tag_number} / ${v2_api_page_size} + 1))
    local v2_api_to_query
    local candidates=""
    local candidates_querying

    for (( page_number=1; page_number<=${last_page_number}; page_number++ ))
    do
        v2_api_to_query=${v2_api}'?&'page=${page_number}'&page_size='${v2_api_page_size}
        candidates_querying=$( curl -L -s ${v2_api_to_query} | \
            jq '."results"[]["name"]' | \
            tr -d \" | \
	    grep -E "^${branch}(\.[0-9]+)*-devel-ubuntu${LXD_OS_VER}$" )
        candidates="${candidates} ${candidates_querying}"
    done
    # candidates is a string with space delimiter. use tr to translate spaces into
    # carriage returns that will be used by sort
    #
    # example: echo 11.4.2-devel-ubuntu20.04 11.4.0-devel-ubuntu20.04
    # 11.4.1-devel-ubuntu20.04 11.4.3-devel-ubuntu20.04 | tr ' ' '\n' | sort -n
    #
    # then tail -1 will get the latest one e.g. 11.4.3-devel-ubuntu20.04
    echo ${candidates} | tr " " "\n" | sort -n | tail -1
}

gen_vars() {
    local cuda_branch
    local container_tag

    cuda_branch=11.4
    container_tag="$(find_latest_cuda_container_tag_by_branch "$cuda_branch")"
    echo ${container_tag}
}

gen_vars
