#!/bin/bash
#
# usage: invoke anywhere in the system by
#   ${HOME}/toolbox/scripts/a-c-t-testing-03-nv-server-driver/02-setup-autotest-dgx.sh
#

set -e

RELEASE_SHORT=$( lsb_release -rs )
PKGS_BASIC="git gdb"
PKGS="${PKGS_BASIC}"

#ACT_REPO="https://gitlab.com/tai271828/autotest-client-tests.git"
ACT_REPO="https://git.launchpad.net/~canonical-kernel-team/+git/autotest-client-tests"
#ACT_BRANCH="staging"
ACT_BRANCH="master"

pushd "${HOME}"

if [ "${RELEASE_SHORT}" == "22.04" ]
then
  # hmm after running a-c-t with jammy, I guess only python2 is only used for customized jobs but not the autotest core
  #
  #  09:29:40 DEBUG| Persistent state client._record_indent now set to 0
  #  09:29:40 INFO | Report successfully generated at /home/ubuntu/autotest/client/results/default/job_report.html
  #  Linux hot-koala 5.15.0-27-generic #28-Ubuntu SMP Thu Apr 14 04:55:28 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
  #  No LSB modules are available.
  #  Distributor ID: Ubuntu
  #  Description:    Ubuntu 22.04 LTS
  #  Release:        22.04
  #  Codename:       jammy
  PKGS="python3-yaml ${PKGS}"
elif [ "${RELEASE_SHORT}" == "20.04" ]
then
  PKGS="python2 python-yaml ${PKGS}"
else
  PKGS="python python-yaml ${PKGS}"
fi

sudo apt-get update
sudo apt-get install ${PKGS} -y

popd
