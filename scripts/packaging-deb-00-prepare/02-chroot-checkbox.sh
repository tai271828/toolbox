#!/usr/bin/env bash
#
# invoke this script when we want to customize the chroot env for checkbox
#
DIR_CHROOT=${HOME}/debootstrap-chroot-ubuntu-focal
PATH_SRC_CHECKBOX_APT_SRC_LIST=/etc/apt/sources.list.d/checkbox-dev-ubuntu-ppa-focal.list
PATH_SRC_APT_SRC_LIST=/etc/apt/sources.list

sudo add-apt-repository ppa:checkbox-dev/ppa -y
sudo apt update
sudo apt install -y checkbox-ng plainbox-provider-checkbox canonical-certification-server

sudo cp "${PATH_SRC_CHECKBOX_APT_SRC_LIST}" "${DIR_CHROOT}"/"${PATH_SRC_CHECKBOX_APT_SRC_LIST}"
sudo cp "${PATH_SRC_APT_SRC_LIST}" "${DIR_CHROOT}"/"${PATH_SRC_APT_SRC_LIST}"

# PGP key
# random number

schroot -c UNRELEASED -u root -- sh -c "apt update"
schroot -c UNRELEASED -u root -- sh -c "apt install gpg -y"
schroot -c UNRELEASED -u root -- sh -c "apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1B4B6B2D2BBDF2BD"
schroot -c UNRELEASED -u root -- sh -c "apt update"
schroot -c UNRELEASED -u root -- sh -c "apt install -y intltool dose-distcheck build-essential fakeroot \
                                                       plainbox-provider-resource-generic \
                                                       python3-checkbox-support \
                                                       python3-checkbox-ng"