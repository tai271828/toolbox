#!/usr/bin/env bash
#
# Enable proposal and then install the kernel deb
#

# Using $EUID may be better than "/usr/bin/id -u" for less commands.
# ref: https://serverfault.com/questions/37829/bash-scripting-require-script-to-be-run-as-root-or-with-sudo
if [[ $EUID -ne 0 ]]
then
    echo "$0 is not running as super user (root). Try sudo $0"
    # misuse of shell built-in.
    # maybe 126 is applicable as well (cannot execute)
    exit 2
fi

# ref: https://serverfault.com/questions/37829/bash-scripting-require-script-to-be-run-as-root-or-with-sudo
cat <<EOF >/etc/apt/sources.list.d/ubuntu-$(lsb_release -cs)-proposed.list
# Enable Ubuntu proposed archive
deb http://ports.ubuntu.com/ubuntu-ports $(lsb_release -cs)-proposed restricted main multiverse universe
EOF

apt update

# linux-generic is a meta packages that will help you install all necessary packages.
#
# For example:
#     ubuntu@segers:~$ sudo apt-get install linux-generic
#     ubuntu@segers:~$ dpkg -l *5.13.0-28*
#     Desired=Unknown/Install/Remove/Purge/Hold
#     | Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
#     |/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
#     ||/ Name                                   Version      Architecture Description
#     +++-======================================-============-============-==========================================================
#     ii  linux-headers-5.13.0-28                5.13.0-28.31 all          Header files related to Linux kernel version 5.13.0
#     ii  linux-headers-5.13.0-28-generic        5.13.0-28.31 arm64        Linux kernel headers for version 5.13.0 on ARMv8 SMP
#     ii  linux-image-5.13.0-28-generic          5.13.0-28.31 arm64        Signed kernel image generic
#     un  linux-image-unsigned-5.13.0-28-generic <none>       <none>       (no description available)
#     ii  linux-modules-5.13.0-28-generic        5.13.0-28.31 arm64        Linux kernel extra modules for version 5.13.0 on ARMv8 SMP
#     ii  linux-modules-extra-5.13.0-28-generic  5.13.0-28.31 arm64        Linux kernel extra modules for version 5.13.0 on ARMv8 SMP
#     ubuntu@segers:~$ dpkg -l *5.13.0-30*
#     Desired=Unknown/Install/Remove/Purge/Hold
#     | Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
#     |/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
#     ||/ Name                                   Version      Architecture Description
#     +++-======================================-============-============-==========================================================
#     ii  linux-headers-5.13.0-30                5.13.0-30.33 all          Header files related to Linux kernel version 5.13.0
#     ii  linux-headers-5.13.0-30-generic        5.13.0-30.33 arm64        Linux kernel headers for version 5.13.0 on ARMv8 SMP
#     ii  linux-image-5.13.0-30-generic          5.13.0-30.33 arm64        Signed kernel image generic
#     un  linux-image-unsigned-5.13.0-30-generic <none>       <none>       (no description available)
#     ii  linux-modules-5.13.0-30-generic        5.13.0-30.33 arm64        Linux kernel extra modules for version 5.13.0 on ARMv8 SMP
#     ii  linux-modules-extra-5.13.0-30-generic  5.13.0-30.33 arm64        Linux kernel extra modules for version 5.13.0 on ARMv8 SMP
apt install linux-generic

# reboot to take the update effective
