#!/bin/bash

echo ''
echo '==============================='
echo 'Begin converting......'
echo '==============================='

echo ''
echo 'The following files are going to be converted'

for file in `ls *.MOV`
do
            echo $file
    done

    echo ''

    for file in `ls *.MOV`
    do
                echo "Converting $file ......"
                    ffmpeg -i $file -s 480x270  -b 64k -vcodec mpeg4 -acodec aac -strict experimental out-${file}
                        echo "Converting $file done!"
                done

                echo '==============================='
                echo 'End!'
                echo '==============================='
                echo ''
