#!/usr/bin/bash
set -e

export poetry_root=${HOME}/work-my-projects/solvcon/modmesh-venv-py3.9.13
export devenv_root=${HOME}/work-my-projects/solvcon/devenv
export devenv_modmesh_env_name="modmesh-01"
export pybind11_DIR=${HOME}/work-my-projects/solvcon/modmesh-installation

# poetry will respwan new shell session so source this script will stop at this step
# use "source activate" instead
pushd ${poetry_root}
python_venv_root=$(poetry env info --path)
source ${python_venv_root}/bin/activate
popd

pushd ${devenv_root}
source scripts/init
devenv list
devenv use ${devenv_modmesh_env_name}
popd

set +e
