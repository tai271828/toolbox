#!/usr/bin/python

output_filename = 'thho-output.srt'
input_sub_en_filename = 'fresh_sub_en.txt'
input_sub_ch_filename = 'fresh_sub_ch.txt'

# in sec
time_shift = 8

# will give you a dictionary
# {'time':'sub text'}
def prepare_sub(filename):
    sub_dic = {}

    with open(filename) as input_file:
        data = input_file.readlines()

    for line in data:
        semi_sub = line.split("\t")
        if len(semi_sub) == 2:
            sub_dic[semi_sub[0].rstrip()] = semi_sub[1].rstrip()
    return sub_dic

# gen the sequence
def sort_timestamp(sub_dic_a, sub_dic_b):
    timestamp_list = []
    for x in sub_dic_a.keys():
        if sub_dic_b.has_key(x):
            timestamp_list.append(x)
        else:
            print x
    timestamp_list.sort()

    return timestamp_list

def get_shift_time(time):
    # 01.02.19
    # 00:04:52,700 --> 00:04:58,300
    time = time[3:]
    shifted_time = time.split('.')
    total_sec = int(shifted_time[0])*60 + int(shifted_time[1]) - time_shift
    sec = total_sec % 60
    minute = (total_sec - sec)/60
    #start_time = '00:' + time.replace('.',':') + ',000'
    #end_time = '00:' + time.replace('.',':') + ',900'
    mm = str(minute).zfill(2)
    ss = str(sec).zfill(2)
    start_time = '00:' + mm + ':' + ss + ',000'
    end_time =   '00:' + mm + ':' + ss + ',900'
    return start_time, end_time

# based on sorting result
# merge eng and chi subtitles in srt format
def gen_srt(sorted_timestamp, sub_dic):
    with open(output_filename, 'w') as output_file:
        for i,time in enumerate(sorted_timestamp,1):
            if sub_dic.has_key(time):
                output_file.write(str(i) + "\n")
                start_time, end_time = get_shift_time(time)
                output_file.write(start_time + ' --> ' + end_time + "\n")
                output_file.write(sub_dic[time] + "\n\n")




sub_dic_eng = prepare_sub(input_sub_en_filename)
sub_dic_chi = prepare_sub(input_sub_ch_filename)

# why the length of sub_dic_eng is different from sub_dic_chi
# because there are typo or missing in the raw input files
# please compare the length of sub_dic_eng and sorted_timestamp to
# find out the difference
sorted_timestamp = sort_timestamp(sub_dic_eng, sub_dic_chi)
#print '--------'
#sorted_timestamp2 = sort_timestamp(sub_dic_chi, sub_dic_eng)
gen_srt(sorted_timestamp, sub_dic_chi)


