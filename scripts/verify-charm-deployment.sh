#!/usr/bin/env bash
SRC_SCALEBOT_JENKINS="${HOME}/work/scalebot-projects/scalebot-jenkins"
SRC_SCALEBOT="${HOME}/work/scalebot-projects/scalebot"
CHARM_NAME=ce-hyperscale-scalebot-jenkins_ubuntu-20.04-amd64-arm64.charm

# build/pack the charm
#pushd ${SRC_SCALEBOT_JENKINS}
#charmcraft pack
#mv ${CHARM_NAME} ../
#popd

# purge juju configuration for local lxd
# and re-boostrap
#
# lxd bridge ip may change for each system reboot, so re-do the infra setup
lxc list

echo
rm -rf ${HOME}/.local/share/juju
juju bootstrap localhost local-scalebot-lxd
echo

lxc list
echo
juju status

# change config
echo
echo "vi ${SRC_SCALEBOT}/common_bundle.yaml"
echo "for"
echo "    scalebot:charm: ${HOME}/work/scalebot-projects/${CHARM_NAME}"
echo
echo "vi ${SRC_SCALEBOT}/labs/remote-lxd/clouds.yaml"
echo "for"
echo "    endpoint: https://10.94.220.1:8443"
echo

cd ${SRC_SCALEBOT}
./deploy.py -l remote-lxd -c scalebot
watch -n1 --color juju status --color
